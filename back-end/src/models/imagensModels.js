var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var imagensSchema = new Schema({
    hrefFoto: {type: String}
})

module.exports = mongoose.model("imagens", imagensSchema);