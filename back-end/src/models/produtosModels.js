var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var produtosSchema = new Schema({
    nome: {type: String},
    preco: {type: Number},
    id_grupo: {
        type: Schema.Types.ObjectId,
        ref: "grupos"
    },
    id_foto: {
        type: Schema.Types.ObjectId,
        ref: "imagens"
    }
})

module.exports = mongoose.model("produtos", produtosSchema);