var Pedido = require("../models/pedidosModels");
const jwt = require('jsonwebtoken');

exports.findAllPedido = function (req,res) {
    Pedido.find({}, (err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

exports.findAllDetailsPedido = function (req,res) {
    Pedido.find({}).populate({ path: 'produtos.id_produto', populate: { path: 'id_foto' } }).populate('id_cliente').exec((err, values) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            });
        } else {
            res.status(200).json({
                "dados": values
            });
        }
    });

};

exports.inserirPedido = function (req, res) {

    let { produtos, token, forma_pagamento } = req.body;

    if (produtos == undefined || token == undefined ||forma_pagamento == undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {

        let tokenDecoded = jwt.verify(token, "hipropa");

        produtos = JSON.parse(produtos);

        const pedido = new Pedido({
            'produtos': produtos,
            'id_cliente': tokenDecoded['_id'],
            'forma_pagamento': forma_pagamento
        });
        
        pedido.save((err, result) => {
            if (err) {
                res.status(406).json({
                    'response': null,
                    'error': err
                })
            } else {
                res.status(200).json({
                    'error': null,
                    'response': {
                        'message': 'success'
                    }
                });
            }
        });
    }

};

exports.editarPedido = function (req,res) {
    let data = {
        _id: null,
        id_produto: null,
    };
    for (let i in req.body['data']) {
        if (i in data) {
            data[i] = req.body['data'][i];
        }
    }
    removeNull(data);
    
    Pedido.updateOne({
        _id: data['_id']
    }, data, function (err,raw) {
        if (err) {
            return res.status(406).json({
                'response': null,
                'error': err
            })
        }
        else {
            return res.status(200).json({
                'error': null,
                'response': {
                    'message': raw
                }
            })
        }
    }
    )
}

removeNull = function( data ) {
    for (let i in data) {
        if (data[i] == null) {
            delete data[i];
        }
    }
}

exports.buscaPedidoByID = function (req, res) {

    const { token } = req.body;

    if (token == undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {

        let tokenDecoded = jwt.verify(token, 'hipropa');

        Pedido.find({
            id_cliente: tokenDecoded['_id']
        })
        .sort({ data_pedido: -1 })
        .populate({ path: 'produtos.id_produto', populate: [{ path: 'id_grupo', populate: { path: 'id_foto' } }, { path: 'id_foto' }] })
        .exec((err, result) => {
            if (err) {
                res.status(406).json({
                    'response': null,
                    'error': err
                })
            } else {
                res.status(200).json({
                    'error': null,
                    'response': result
                })
            }
        });

    }
};

exports.buscaPedidoByNome = function (req, res) {
    Pedido.find({
        id_produto: req.body.id_produto
    }, (err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

exports.deletarPedido = function (req, res) {
    try {
        Pedido.deleteOne({
            _id: req.body.id,
        }, (err, result) => {
            if (err) {
                res.status(500).json({
                    'response': null,
                    'error': err
                })
            } else if (result.n == 1) {
                res.status(200).json({
                    'error': null,
                    'response': {
                        'message': 'success'
                    },
                })
            } else {
                res.status(500).json({
                    'response': null,
                    'error': err
                })
            }
        })
    } catch (error) {
        console.log("Erro")
    }
};