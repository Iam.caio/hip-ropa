var Imagem = require("../models/imagensModels");

exports.findAllImagem = function (req,res) {
    Imagem.find({}, (err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

exports.inserirImagem = function (req, res) {
    const imagem = new Imagem({
        hrefFoto: req.body.hrefFoto
    })
    imagem.save((err, result) => {
            if (err) {
                res.status(406).json({
                    'response': null,
                    'error': err
                })
            } else {
                res.status(200).json({
                    'error': null,
                    'response': {
                        'message': 'success'
                    }
                });
            }
        })
};

exports.editarImagem = function (req,res) {
    let data = {
        _id: null,
        hrefFoto: null,
    };
    for (let i in req.body['data']) {
        if (i in data) {
            data[i] = req.body['data'][i];
        }
    }
    removeNull(data);
    
    Imagem.updateOne({
        _id: data['_id']
    }, data, function (err,raw) {
        if (err) {
            return res.status(406).json({
                'response': null,
                'error': err
            })
        }
        else {
            return res.status(200).json({
                'error': null,
                'response': {
                    'message': raw
                }
            })
        }
    }
    )
}

removeNull = function( data ) {
    for (let i in data) {
        if (data[i] == null) {
            delete data[i];
        }
    }
}

exports.buscaImagemByID = function (req, res) {
    Imagem.findById({
        _id: req.params.id
    }, (err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

exports.buscaImagemByNome = function (req, res) {
    Imagem.find({
        hrefFoto: req.body.hrefFoto
    }, (err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    })
};

exports.deletarImagem = function (req, res) {
    try {
        Imagem.deleteOne({
            _id: req.body.id,
        }, (err, result) => {
            if (err) {
                res.status(500).json({
                    'response': null,
                    'error': err
                })
            } else if (result.n == 1) {
                res.status(200).json({
                    'error': null,
                    'response': {
                        'message': 'success'
                    },
                })
            } else {
                res.status(500).json({
                    'response': null,
                    'error': err
                })
            }
        })
    } catch (error) {
        console.log("Erro")
    }
};