var Sensor = require("../models/sensorModels");

exports.inserirSensor = function (req, res) {

    console.log('chamou');

    const sensor = new Sensor({
        temperatura: req.body.temperatura
    })
    sensor.save((err, result) => {
            if (err) {
                res.status(406).json({
                    'response': null,
                    'error': err
                })
            } else {
                res.status(200).json({
                    'error': null,
                    'response': {
                        'message': 'success'
                    }
                });
            }
        })
};

exports.buscaSensor = function (req, res) {
    Sensor
    .find()
    .sort({ dataHora: -1 })
    .limit(1)
    .exec((err, result) => {
        if (err) {
            res.status(406).json({
                'response': null,
                'error': err
            })
        } else {
            res.status(200).json({
                'error': null,
                'response': result
            })
        }
    });
};