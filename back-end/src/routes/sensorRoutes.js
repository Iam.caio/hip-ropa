const express = require("express");
const router = express.Router();
var sensorController = require('../controllers/sensorController');

router.get("/buscar/", sensorController.buscaSensor)
router.post("/inserir/", sensorController.inserirSensor)

module.exports = router