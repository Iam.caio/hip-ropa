const express = require("express");
const router = express.Router();
var pedidoController = require('../controllers/pedidosControllers');

router.get("/buscar", pedidoController.findAllPedido)
router.get("/buscar/details/", pedidoController.findAllDetailsPedido)
router.post("/buscar", pedidoController.buscaPedidoByID)
router.post("/buscar/nome/", pedidoController.buscaPedidoByNome)
router.post("/inserir/", pedidoController.inserirPedido)
router.post("/editar/", pedidoController.editarPedido)
router.post("/deletar/",pedidoController.deletarPedido)

module.exports = router