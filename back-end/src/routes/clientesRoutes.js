const express = require("express");
const router = express.Router();
var clienteController = require('../controllers/clientesControllers');

router.get("/buscar", clienteController.findAllCliente);
router.post("/buscar/:id", clienteController.buscaClienteByID);
router.post("/buscar/nome/", clienteController.buscaClienteByNome);
router.post("/inserir", clienteController.inserirCliente);
router.post("/login", clienteController.login);
router.post("/editar/", clienteController.editarCliente);
router.post("/deletar/", clienteController.deletarCliente);

module.exports = router