const express = require("express");
const router = express.Router();
var produtoController = require('../controllers/produtosControllers');

router.get("/buscar", produtoController.findAllProduto)
// router.get("/buscar/details", produtoController.findAllDetailsProdutos)
router.get("/buscar/:id_group/:limite", produtoController.buscaProdutoByID)
router.post("/buscar/nome", produtoController.buscaProdutoByNome)
router.post("/inserir", produtoController.inserirProduto)
router.post("/editar", produtoController.editarProduto)
router.post("/deletar", produtoController.deletarProduto)

module.exports = router