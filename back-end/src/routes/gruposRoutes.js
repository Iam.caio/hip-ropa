const express = require("express");
const router = express.Router();
var grupoController = require('../controllers/gruposControllers');

router.get("/buscar", grupoController.findAllGrupo)
router.get("/buscar/details", grupoController.findAllDetailsGrupo)
router.post("/buscarid", grupoController.buscaGrupoByID)
router.get("/off", grupoController.getOff);
router.post("/buscar/nome", grupoController.buscaGrupoByNome)
router.post("/inserir", grupoController.inserirGrupo)
router.post("/editar", grupoController.editarGrupo)
router.post("/deletar",grupoController.deletarGrupo)

module.exports = router