const express = require("express")
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const clientesRoutes = require("./src/routes/clientesRoutes");
const grupoRoutes = require("./src/routes/gruposRoutes");
const produtoRoutes = require("./src/routes/produtosRoutes");
const pedidosRoutes = require("./src/routes/pedidosRoutes");
const imagensRoutes = require("./src/routes/imagensRoutes");
const sensorRoutes = require("./src/routes/sensorRoutes");
const carrinhoRoutes = require("./src/routes/carrinhoRoutes");

mongoose.connect('mongodb+srv://admHipRopa:bc123456@hip-ropa-s9hbg.mongodb.net/HipRopa', { useNewUrlParser: true, useUnifiedTopology: true }); 
mongoose.Promise = global.Promise;

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Pass to next layer of middleware
    next();
});

app.use("/client", clientesRoutes);
app.use("/group", grupoRoutes);
app.use("/product", produtoRoutes);
app.use("/order", pedidosRoutes);
app.use("/image", imagensRoutes);
app.use("/sensor", sensorRoutes);
app.use("/cart", carrinhoRoutes);

app.use((req, res, next) => {
    const erro = new Error("Não encontrado");
    erro.status = 404;
    next(erro);
})

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        erro: {
            mensagem: error.message
        }
    })
})

module.exports = app;