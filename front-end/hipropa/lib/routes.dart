import 'package:flutter/material.dart';
import 'package:hipropa/pages/auth.dart';
import 'package:hipropa/pages/homepage.dart';
import 'package:hipropa/pages/splash/splash.dart';

final routes = {
  '/': (BuildContext context) => SplashScreen(),
  '/login': (BuildContext context) => AuthPage(),
  '/home': (BuildContext context) => HomePage(),
  // '/esqueci-senha': (BuildContext context) => EsqueciSenha(),
  // '/cadastro': (BuildContext context) => CadastroUsuario()
};
