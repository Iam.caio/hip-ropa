import 'package:hipropa/models/category.dart';

class ProductModel {
  String id;
  String name;
  String description;
  String image;
  double price;
  int qtd;
  CategoryModel category;

  ProductModel({
    this.id,
    this.name,
    this.image,
    this.price,
    this.category,
    this.description,
    this.qtd
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return new ProductModel(
      id: json['_id'].toString(),
      name: json['nome'],
      image: json['id_foto']['hrefFoto'],
      price: json['preco'].toDouble(),
      description: json['descricao'],
      category: new CategoryModel.fromJson(json['id_grupo'])
    );
  }

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
      'description': description,
      'image': image,
      'price': price,
      'category': category.toJson()
    };


}