class CategoryModel {
  String id;
  String name;
  String image;

  CategoryModel({
    this.id,
    this.name,
    this.image,
  });

  factory CategoryModel.fromJson(Map<String, dynamic> json) {
    return new CategoryModel(
      id: json['_id'].toString(),
      name: json['nomeGrupo'],
      image: json['id_foto']['hrefFoto']
    );
  }

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
      'image': image
    };

}