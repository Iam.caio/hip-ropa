
import 'dart:convert';
import 'dart:io';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:hipropa/models/product.dart';
import 'package:hipropa/models/user.dart';
import 'package:hipropa/utils/api.dart';
import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class UserBloc implements BlocBase {
  API api;

  UserModel _userData;
  bool isLogged;

  UserBloc() {

    api = API();

    SharedPreferences.getInstance().then((prefs) {
      if (prefs.getKeys().contains("user")) {
        this._userData = UserModel.fromJson(json.decode(prefs.getString('user')));
        this.isLogged = json.decode(prefs.getString('isLogged'));
      }
    });

  }

  UserModel get data => _userData;

  login(UserModel user, BuildContext context, GlobalKey<ScaffoldState> _keyScaffold) async {
    api.login(user)
    .then((response) {
      if (response.statusCode == HttpStatus.created) {
        final data = json.decode(response.body);
        if (data.length != 0) {
          this.isLogged = true;
          this._userData = UserModel.fromJson(data['response']['data']);
          saveUser();
          Navigator.of(context).pushNamed('/home');
        } else {
          snackBarInfoError(context, _keyScaffold, "Falha no Login!");
        }
      } else {
        snackBarInfoError(context, _keyScaffold, "Falha no Login!");
      }
    })
    .catchError((err) {
      snackBarInfoError(context, _keyScaffold, "Falha no Login!");
    });
  }

  register(UserModel user, BuildContext context, GlobalKey<ScaffoldState> _keyScaffold) async {
    api.register(user)
    .then((response) {
      if (response.statusCode == HttpStatus.created) {
        final data = json.decode(response.body);
        print(data['response']['data']);
        this.isLogged = true;
        this._userData = UserModel.fromJson(data['response']['data']);
        saveUser();
        Navigator.of(context).pushNamed('/home');
      } else {
        snackBarInfoError(context, _keyScaffold, "Falha ao cadastrar!");
      }
    })
    .catchError((err) {
      snackBarInfoError(context, _keyScaffold, "Falha ao cadastrar!");
    });
  }

  logout(BuildContext context) async {
    this._userData = new UserModel();
    this.isLogged = false;
    saveUser();
    Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
  }

  snackBarInfoError(BuildContext context, GlobalKey<ScaffoldState> _keyScaffold, String msg) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _keyScaffold.currentState.showSnackBar(SnackBar(
      content: Text(msg),
      backgroundColor: Colors.green,
      duration: Duration(seconds: 4),
    ));
  }

  saveUser() async {
    await SharedPreferences.getInstance().then((prefs) {
      prefs.setString("user", json.encode(this._userData.toJson()));
      prefs.setString("isLogged", json.encode(isLogged));
    });
  }

  @override
  void dispose() {
    
  }

}