import 'dart:convert';

import 'package:hipropa/models/category.dart';
import 'package:hipropa/models/product.dart';
import 'package:hipropa/models/user.dart';
import 'package:http/http.dart' as http;

class API {
// 
  String url = 'http://10.64.162.167:8080/';
  // String url = 'http://192.168.0.110:8080/';
  // String url = 'http://192.168.0.24:3000/';

  Future<http.Response> insertProduct(ProductModel product) async {
    Map<String, dynamic> bodyPost = product.toJson();

    http.Response response =
        await http.post(url + 'products/', body: bodyPost);
    return response;
  }

  Future<http.Response> getAllProducts() async {
    http.Response response = await http.get(url + 'product/buscar');
    return response;
  }

  Future<http.Response> getProductsByCategory(CategoryModel categoryModel, int limit) async {
    http.Response response = await http.get(url + 'product/buscar/' + categoryModel.id + '/' + limit.toString());
    return response;
  }

  Future<http.Response> getCategories() async {
    http.Response response = await http.get(url + 'group/buscar');
    return response;
  }

  Future<http.Response> getCategoriesOFF() async {
    http.Response response = await http.get(url + 'group/off');
    return response;
  }

  Future<http.Response> login(UserModel user) async {
    // http.Response response = await http.post(url + 'login', body: { 'user': user['email'], 'password': user['password'] });
    http.Response response = await http.post(url + 'client/login', body: user.toJsonLogin());
    return response;
  } 

  Future<http.Response> register(UserModel user) async {
    http.Response response = await http.post(url + 'client/inserir', body: user.toJsonRegister());
    return response;
  }

  Future<http.Response> getOrders(UserModel user) async {
    http.Response response = await http.post(url + 'order/buscar', body: { "token": user.token });
    return response;
  }

  Future<http.Response> setOrder(UserModel user, List products) async {
    http.Response response = await http.post(url + 'order/inserir', body: { "token": user.token, "forma_pagamento": "cartão", "produtos": json.encode(products) });
    return response;
  }

  Future<http.Response> getShoppingCart(UserModel user) async {
    http.Response response = await http.post(url + 'cart/find', body: { "token": user.token });
    return response;
  }

  Future<http.Response> setShoppingCart(UserModel user, Map<String, dynamic> cart) async {
    http.Response response = await http.post(url + 'cart/insert', body: { "token": user.token, "carrinho": json.encode(cart) });
    return response;
  }

  Future<http.Response> cleanShoppingCart(UserModel user) async {
    http.Response response = await http.post(url + 'cart/clear', body: { "token": user.token } );
    return response;
  }

  Future<http.Response> getTemperatura() async {
    http.Response response = await http.get(url + 'sensor/buscar');
    return response;
  }

}