import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hipropa/blocs/user.dart';
import 'package:hipropa/models/user.dart';

class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {

  GlobalKey<FormState> _keyForm = new GlobalKey<FormState>();
  GlobalKey<FormState> _keyFormRegister = new GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _keyScaffold = new GlobalKey<ScaffoldState>();
  
  int page = 0;

  UserModel user = new UserModel();
  UserBloc userBloc;

  final _nameFocus = FocusNode();
  final _emailFocus = FocusNode();
  final _passwordFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    this.userBloc = BlocProvider.of<UserBloc>(context);
    return Scaffold(
      key: _keyScaffold,
      // appBar: AppBar(
      //   elevation: 0,
      //   backgroundColor: Colors.white,
      // ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[

            Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.width * 0.1
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.shopping_cart, color: Colors.green, size: MediaQuery.of(context).size.height * 0.2)
                ],
              ),
            ),
            Container(
              alignment: Alignment.topCenter,
              child: page == 0 ? loginWidget() : registerWidget()
            ),
          ],
        )
      )
    );
  }

  Widget registerWidget() {
    return Form(
      key: _keyFormRegister,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(16),
                child: FlatButton(
                  onPressed: () {
                    setState(() {
                      this.page = 0;
                    });
                  },
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.green,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(16),
                child: FlatButton(
                  onPressed: null,
                  child: Text(
                    'Register',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 16, top: 8),
            child: Text(
              'Bem-vindo a Hip-Ropa.',
              style:
                  TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 16, top: 8),
            child: Text(
              'Vamos começar!',
              style: TextStyle(
                  fontSize: 18, fontWeight: FontWeight.normal),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                left: 16, right: 16, top: 32, bottom: 8),
            child: TextFormField(
              focusNode: _nameFocus,
              validator: (value) {
                if (value.isEmpty) return 'Nome inválido';
                else return null;
              },
              onSaved: (value) {
                this.user.name = value;
              },
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (val) {
                _nameFocus.unfocus();
                FocusScope.of(context).requestFocus(_emailFocus);
              },
              style: TextStyle(fontSize: 18),
              textCapitalization: TextCapitalization.words,
              decoration: InputDecoration(
                hintText: 'Nome',
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Colors.grey)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Colors.grey)),
              ),
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
            child: TextFormField(
              focusNode: _emailFocus,
              validator: (value) {
                if (!value.contains('@') && !value.contains('.')) return 'E-mail inválido';
                else return null;
              },
              onSaved: (value) {
                this.user.email = value;
              },
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (val) {
                _emailFocus.unfocus();
                FocusScope.of(context).requestFocus(_passwordFocus);
              },
              inputFormatters: [
                BlacklistingTextInputFormatter(new RegExp('[\\,|\\ ]'))
              ],
              style: TextStyle(fontSize: 18),
              decoration: InputDecoration(
                hintText: 'E-Mail',
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Colors.grey)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Colors.grey)),
              ),
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
            child: TextFormField(
              validator: (value) {
                if (value.length < 6) return 'A senha precisa de mais de 6 digitos.';
                else return null;
              },
              onSaved: (value) {
                this.user.password = value;
              },
              obscureText: true,
              focusNode: _passwordFocus,
              style: TextStyle(fontSize: 18),
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.done,
              onFieldSubmitted: (val) {
                _passwordFocus.unfocus();
                validateRegister();
              },
              decoration: InputDecoration(
                hintText: 'Senha',
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Colors.grey)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Colors.grey)),
              ),
            ),
          ),
          Align(
              alignment: Alignment.centerRight,
              child: Container(
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    color: Colors.green, shape: BoxShape.circle),
                child: IconButton(
                  color: Colors.white,
                  onPressed: validateRegister,
                  icon: Icon(Icons.arrow_forward),
                ),
              )
            ),
        ],
      )
    );
  }

  Widget loginWidget() {
    return Form(
      key: _keyForm,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(16),
                child: FlatButton(
                  onPressed: null,
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(16),
                child: FlatButton(
                  onPressed: () {
                    setState(() {
                      this.page = 1;
                    });
                  },
                  child: Text(
                    'Register',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.green,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 16),
            child: Container(
              margin: EdgeInsets.only(left: 16, top: 8),
              child: Text(
                'Bem-vindo a Hip-Ropa.',
                style:
                    TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              ),
            )
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 16,
              right: 16,
              top: 8,
              bottom: 8
            ),
            child: TextFormField(
              focusNode: _emailFocus,
              validator: (value) {
                if (!value.contains('@') && !value.contains('.')) return 'E-mail inválido';
                else return null;
              },
              onSaved: (value) {
                this.user.email = value;
              },
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (val) {
                _emailFocus.unfocus();
                FocusScope.of(context).requestFocus(_passwordFocus);
              },
              inputFormatters: [
                BlacklistingTextInputFormatter(new RegExp('[\\,|\\ ]'))
              ],
              style: TextStyle(fontSize: 18),
              decoration: InputDecoration(
                hintText: 'E-mail',
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Colors.grey)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: Colors.grey)),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: 16,
              right: 16,
              top: 8,
              bottom: 8
            ),
            child: TextFormField(
              validator: (value) {
                if (value.length < 6) return 'A senha precisa de mais de 6 digitos.';
                else return null;
              },
              onSaved: (value) {
                this.user.password = value;
              },
              obscureText: true,
              focusNode: _passwordFocus,
              style: TextStyle(fontSize: 18),
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.done,
              onFieldSubmitted: (val) {
                _passwordFocus.unfocus();
                validateLogin();
              },
              decoration: InputDecoration(
                hintText: 'Senha',
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.grey)
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(color: Colors.grey)
                ),
              ),
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FlatButton(
                onPressed: () {},
                child: Text(
                  "Esqueci a Senha",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18
                  ),
                )
              ),
              Container(
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    color: Colors.green, shape: BoxShape.circle),
                child: IconButton(
                  color: Colors.white,
                  onPressed: validateLogin,
                  icon: Icon(Icons.arrow_forward)
                ),
              )
            ],
          )
        ],
      )
    );
  }

  validateLogin() async {
    print('chamou a');
    if (_keyForm.currentState.validate()) {
      _keyForm.currentState.save();
      await this.userBloc.login(user, context, _keyScaffold);
    } else {
      await this.userBloc.snackBarInfoError(context, _keyScaffold, "Falha no Login!");
    }
  }

  validateRegister() async {
    print('chamou');
    if (_keyFormRegister.currentState.validate()) {
      _keyFormRegister.currentState.save();
      print('chamou');
      await this.userBloc.register(this.user, context, _keyScaffold);
    } else {
      print('Falhou');
      await this.userBloc.snackBarInfoError(context, _keyScaffold, "Falha no Cadastro!");
    }
  }

}