import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:hipropa/blocs/product.dart';
import 'package:hipropa/models/category.dart';
import 'package:hipropa/models/product.dart';
import 'package:hipropa/pages/product.dart';

class CategoryPage extends StatefulWidget {

  CategoryModel category;

  CategoryPage(this.category);

  @override
  _CategoryPageState createState() => _CategoryPageState(this.category);
}

class _CategoryPageState extends State<CategoryPage> {

  CategoryModel category;
  ProductBloc blocProduct;

  _CategoryPageState(this.category);

  @override
  Widget build(BuildContext context) {
    this.blocProduct = BlocProvider.of<ProductBloc>(context);
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        padding: EdgeInsets.all(20.0),
        color: const Color(0xffF4F7FA),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 16, top: 20),
                  child: Text(
                    '${this.category.name}',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                )
              ],
            ),
            Divider(
              height: 20,
            ),
            Expanded(
              child: _buildCategoryList(),
            )
          ],
        ),
      ),
    );
  }

  _buildCategoryList() {
    return FutureBuilder(
      future: this.blocProduct.getProductsByCategory(category, 100),
      initialData: [],
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
          case ConnectionState.none:
            return Container(
              alignment: Alignment.center,
              width: 200.0,
              height: 200.0,
              child: CircularProgressIndicator(
                valueColor:
                    AlwaysStoppedAnimation<Color>(Colors.white),
                strokeWidth: 5.0,
              ),
            );
          default:
            if (snapshot.hasError || !snapshot.hasData) {
              return Container();
            } else {
              return GridView.builder(
                shrinkWrap: true,
                itemCount: snapshot?.data?.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3, childAspectRatio: 0.7),
                itemBuilder: (context, index) {
                  return _buildItem(snapshot.data[index]);
                },
              );
            }
        }
      },
    );
  }

  Widget _buildItem(ProductModel data) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return Product(data);
            }
          )
        );
      },
      child: GridTile(
        child: Column(
          children: <Widget>[
            Image.network(data.image, height: 100,),
            Text(data.name.toString(), overflow: TextOverflow.ellipsis,),
            Text("R\$ ${data.price.toStringAsFixed(2)}")
          ],
        ),
      )
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      centerTitle: true,
      brightness: Brightness.dark,
      elevation: 0,
      backgroundColor: Colors.green,
      automaticallyImplyLeading: false,
      title: Text(
        'Hip-Ropa',
        style: TextStyle(color: Colors.white),
      ),
      leading: IconButton(
        onPressed: () => Navigator.of(context).pop(),
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
      ),
    );
  }

}