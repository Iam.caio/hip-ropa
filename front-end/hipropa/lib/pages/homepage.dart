import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hipropa/blocs/product.dart';
import 'package:hipropa/blocs/user.dart';
import 'package:hipropa/pages/profile.dart';
import 'package:hipropa/utils/cardIcons.dart';
import 'package:hipropa/components/homeList.dart';
import 'package:hipropa/components/card.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  List<Widget> _widgetList = [
    HomeList(),
    CartPage(),
    Profile(),
  ];

  int _index = 0;

  UserBloc userBloc;
  ProductBloc productBloc;

  @override
  Widget build(BuildContext context) {
    this.userBloc = BlocProvider.of<UserBloc>(context);
    this.productBloc = BlocProvider.of<ProductBloc>(context);

    this.productBloc.getCarrinho(this.userBloc.data);
    this.productBloc.checkTemp();

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: _buildAppBar(),
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.green,
          unselectedItemColor: Colors.black,
          type: BottomNavigationBarType.shifting,
          currentIndex: _index,
          onTap: (index) {
            setState(() {
              _index = index;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  CartIcons.home,
                ),
                title: Text('   Store  ', style: TextStyle())),
            BottomNavigationBarItem(
                icon: Icon(
                  CartIcons.cart,
                ),
                title: Text('My Cart', style: TextStyle())),
            BottomNavigationBarItem(
                icon: Icon(
                  CartIcons.account,
                ),
                title: Text(
                  'My Account',
                  style: TextStyle(),
                ))
          ],
        ),
        body: _widgetList[_index],
      )
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      centerTitle: true,
      brightness: Brightness.dark,
      elevation: 0,
      backgroundColor: Colors.green,
      automaticallyImplyLeading: false,
      title: Text(
        'Hip-Ropa',
        style: TextStyle(color: Colors.white),
      ),
      actions: <Widget>[
        InkWell(
          child: Icon(Icons.search, color: Colors.white),
          onTap: () {
            print('foi');
          },
        ),
        SizedBox(
          width: 20,
        ),
        InkWell(
          child: Icon(Icons.exit_to_app, color: Colors.white),
          onTap: () {
            this.userBloc.logout(context);
          },
        ),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: new Text('Você tem certeza?'),
            content: new Text('Você quer sair do App'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Não'),
              ),
              new FlatButton(
                onPressed: () => SystemChannels.platform
                    .invokeMethod('SystemNavigator.pop'),
                child: new Text('Sim'),
              ),
            ],
          ),
    ) ??
    false;
  }


}