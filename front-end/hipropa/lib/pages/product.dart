import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:hipropa/blocs/product.dart';
import 'package:hipropa/blocs/user.dart';
import 'package:hipropa/models/product.dart';

class Product extends StatefulWidget {
  final ProductModel product;

  Product(this.product);

  @override
  _ProductState createState() => _ProductState(this.product);
}

class _ProductState extends State <Product> with TickerProviderStateMixin {
  ProductModel product;
  UserBloc userBloc;
  ProductBloc productBloc;

  final _keyScaffold = new GlobalKey<ScaffoldState>();

  _ProductState(this.product);

  @override
  Widget build(BuildContext context) {
    this.userBloc = BlocProvider.of<UserBloc>(context);
    this.productBloc = BlocProvider.of<ProductBloc>(context);
    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.chevron_left,
            size: 40.0,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Colors.white,
        title: Text(
          "DETALHES DO PRODUTO",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: _buildProductDetailsPage(context),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  _buildProductDetailsPage(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return ListView(
      children: < Widget > [
        Container(
          padding: const EdgeInsets.all(4.0),
            child: Card(
              elevation: 4.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: < Widget > [
                  _buildProductImagesWidgets(),
                  _buildProductTitleWidget(),
                  SizedBox(height: 12.0),
                  _buildPriceWidgets(),
                  // SizedBox(height: 12.0),
                  // _buildDivider(screenSize),
                  SizedBox(height: 12.0),
                  _buildDivider(screenSize),
                  SizedBox(height: 12.0),
                  _buildStyleNoteHeader(),
                  SizedBox(height: 6.0),
                  _buildDivider(screenSize),
                  SizedBox(height: 4.0),
                  _buildStyleNoteData(),
                  SizedBox(height: 20.0),
                  _buildMoreInfoHeader(),
                  SizedBox(height: 6.0),
                  _buildDivider(screenSize),
                  SizedBox(height: 4.0),
                  _buildMoreInfoData(),
                  SizedBox(height: 24.0),
                ],
              ),
            ),
        ),
      ],
    );
  }

  _buildDivider(Size screenSize) {
    return Column(
      children: < Widget > [
        Container(
          color: Colors.grey[600],
          width: screenSize.width,
          height: 0.25,
        ),
      ],
    );
  }

  _buildProductImagesWidgets() {
    // TabController imagesController =
    //   TabController(length: product.images.length, vsync: this);

    return Padding(
      padding: const EdgeInsets.all(16.0),
        child: Container(
          height: 250.0,
          child: Center(
            child: Image.network(
              product.image,
            ),
            // child: DefaultTabController(
            //   length: product.images.length,
            //   child: Stack(
            //     children: < Widget > [
            //       TabBarView(
            //         controller: imagesController,
            //         children: product.images.map(
            //           (image) {
            //             return Image.network(
            //               image.imageURL,
            //             );
            //           },
            //         ).toList(),
            //       ),
            //       Container(
            //         alignment: FractionalOffset(0.5, 0.95),
            //         child: TabPageSelector(
            //           controller: imagesController,
            //           selectedColor: Colors.grey,
            //           color: Colors.white,
            //         ),
            //       )
            //     ],
            //   ),
            // ),
          ),
        ),
    );
  }

  _buildProductTitleWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: Center(
          child: Text(
            //name,
            product.name,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
    );
  }

  _buildPriceWidgets() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: < Widget > [
            Text(
              "\$${product.price}",
              style: TextStyle(fontSize: 16.0, color: Colors.black),
            ),
            SizedBox(
              width: 8.0,
            ),
            Text(
              "\$${product.price}",
              style: TextStyle(
                fontSize: 12.0,
                color: Colors.grey,
                decoration: TextDecoration.lineThrough,
              ),
            ),
            SizedBox(
              width: 8.0,
            ),
          ],
        ),
    );
  }

  _buildDetailsAndMaterialWidgets() {
    TabController tabController = new TabController(length: 2, vsync: this);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: < Widget > [
          TabBar(
            controller: tabController,
            tabs: < Widget > [
              Tab(
                child: Text(
                  "DETAILS",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
              Tab(
                child: Text(
                  "MATERIAL & CARE",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 6.0),
            height: 60.0,
            child: TabBarView(
              controller: tabController,
              children: < Widget > [
                Text(
                  "76% acrylic, 19% polyster, 5% metallic yarn Hand-wash cold",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                Text(
                  "86% acrylic, 9% polyster, 1% metallic yarn Hand-wash cold",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _buildStyleNoteHeader() {
    return Padding(
      padding: const EdgeInsets.only(
          left: 12.0,
        ),
        child: Text(
          "DESCRIÇÃO",
          style: TextStyle(
            color: Colors.grey[800],
          ),
        ),
    );
  }

  _buildStyleNoteData() {
    return Padding(
      padding: const EdgeInsets.only(
          left: 12.0,
        ),
        child: Text(
          product.description == null ?
          "Descrição Indisponível" :
          product.description,
          style: TextStyle(
            color: Colors.grey[600],
          ),
        ),
    );
  }

  _buildMoreInfoHeader() {
    return Padding(
      padding: const EdgeInsets.only(
          left: 12.0,
        ),
        child: Text(
          "MAIS INFORMAÇÕES",
          style: TextStyle(
            color: Colors.grey[800],
          ),
        ),
    );
  }

  _buildMoreInfoData() {
    return Padding(
      padding: const EdgeInsets.only(
          left: 12.0,
        ),
        child: Text("Código do Produto: ${product.id}",
      style: TextStyle(
        color: Colors.grey[600],
      ),
    ),
  );
}

  _buildBottomNavigationBar() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: < Widget > [
          Flexible(
            flex: 2,
            child: RaisedButton(
              onPressed: () {
                final cart = { product.id: 1 };
                this.productBloc.setCarrinho(this.userBloc.data , cart, context, _keyScaffold);
              },
              color: Colors.green,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: < Widget > [
                    Icon(
                      Icons.card_travel,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      "Adicionar ao Carrinho",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}