import 'package:flutter/material.dart';
import 'package:hipropa/blocs/product.dart';
import 'package:hipropa/blocs/user.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:hipropa/models/product.dart';
import 'package:hipropa/pages/product.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  UserBloc userBloc;
  ProductBloc productBloc;

  Widget _buildFullName() {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
    );
    return Text(
      this.userBloc.data.name,
      style: _nameTextStyle,
    );
  }

  Widget _buildSeparator(Size screenSize) {
    return Container(
      width: screenSize.width / 1.1,
      height: 2.0,
      color: Colors.green,
      margin: EdgeInsets.only(top: 4.0),
    );
  }

  Widget _buildGetInTouch(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.only(top: 8.0),
      child: Text(
        "Olá ${this.userBloc.data.name.split(" ")[0]}, vejamos suas compras abaixo!",
        style: TextStyle(fontFamily: 'Roboto', fontSize: 16.0),
      ),
    );
  }

  Widget _builderList(BuildContext context, Size screenSize) {
    return FutureBuilder(
      future: this.productBloc.getOrders(this.userBloc.data),
      initialData: [],
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
          case ConnectionState.none:
            return Container(
              alignment: Alignment.center,
              width: 200.0,
              height: 200.0,
              child: CircularProgressIndicator(
                valueColor:
                    AlwaysStoppedAnimation<Color>(Colors.white),
                strokeWidth: 5.0,
              ),
            );
          default:
            if (snapshot.hasError)
              return Container();
            else {
              if (snapshot.data.length < 1) {
                return Container(
                  child: Padding(
                    padding: EdgeInsets.only(top: 50.0),
                    child: Center(
                      child: Text(
                        "Parece que você ainda não comprou nada :(",
                        style: TextStyle(
                          fontSize: 15.0
                        ),
                      ),
                    ),
                  ),
                );
              } else {
                return ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    var data = snapshot.data[index];
                    var date = DateTime.tryParse(data['data_pedido']).toLocal();
                    var pedidos = data['produtos'];
                    var total = 0.0;
                    return Padding(
                      padding: EdgeInsets.only(
                        top: screenSize.height / 80,
                        left: screenSize.width / 10,
                        right: screenSize.width / 10
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "${date.day}/${date.month}/${date.year} ${date.hour < 10 ? '0' + date.hour.toString() : date.hour}:${date.minute < 10 ? '0' + date.minute.toString() : date.minute}",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Divider(color: Colors.green),
                          GridView.builder(
                            physics: new NeverScrollableScrollPhysics(),
                            shrinkWrap : true,
                            itemCount: pedidos.length + 1,
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3, childAspectRatio: 0.7),
                            itemBuilder: (context, index) {
                              if (index < pedidos.length) {
                                total+= (pedidos[index]['qtd'] * pedidos[index]['produto'].price);
                                return _buildItem(pedidos[index]['produto'], pedidos[index]['qtd']);
                              } else {
                                return GridTile(
                                  child: Column(
                                    children: <Widget>[
                                      Icon(Icons.monetization_on, size: 80, color: Colors.green),
                                      Text("Total:"),
                                      Text(total.toStringAsFixed(2))
                                    ],
                                  ),
                                );
                              }
                            },
                          )
                        ],
                      )
                    );
                  }
                );
              }
            }
          }
      },
    );
  }

  Widget _buildItem(ProductModel data, int qtd) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return Product(data);
            }
          )
        );
      },
      child: GridTile(
        child: Column(
          children: <Widget>[
            Image.network(data.image, height: 100,),
            Text("${qtd.toString()} X ${data.name.toString()}", overflow: TextOverflow.ellipsis,),
            Text("R\$ ${double.parse(data.price.toString()).toStringAsFixed(2)}")
          ],
        ),
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    this.userBloc = BlocProvider.of<UserBloc>(context);
    this.productBloc = BlocProvider.of<ProductBloc>(context);
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: screenSize.height / 15.4),
                  Center(
                    child: _buildFullName(),
                  ),
                  _buildGetInTouch(context),
                  SizedBox(height: 8.0),
                  _buildSeparator(screenSize),
                  SizedBox(height: 10.0),
                  _builderList(context, screenSize)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}