#include <WiFi.h>
#include <HTTPClient.h>
#ifdef __cplusplus
extern "C" {
#endif
uint8_t temprature_sens_read();
#ifdef __cplusplus
}
#endif
uint8_t temprature_sens_read();

float value;
float temp;
 
const char* ssid = "BDAG";
const char* password =  "bdag2018";
 
void setup() {
 
  Serial.begin(115200);
  delay(4000);   //Delay needed before calling the WiFi.begin
 
  WiFi.begin(ssid, password); 
 
  while (WiFi.status() != WL_CONNECTED) { //Check for the connection
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
 
  Serial.println("Connected to the WiFi network");
 
}
 
void loop() {
 
  if(WiFi.status()== WL_CONNECTED) {   //Check WiFi connection status
 
    HTTPClient http;   
 
    http.begin("http://10.64.162.167:8080/sensor/inserir");  //Specify destination for HTTP request
    http.addHeader("Content-Type", "application/json");             //Specify content-type header
  
    double temp = (temprature_sens_read() - 32) / 1.8;
   
    int httpResponseCode = http.POST("{\"temperatura\":\"" + String(temp) + "\"}");   //Send the actual POST request
   
    if(httpResponseCode>0){
   
    String response = http.getString();                       //Get the response to the request
   
    Serial.println(httpResponseCode);   //Print return code
    Serial.println(response);           //Print request answer
   
   } else {
 
    Serial.print("Error on sending POST: ");
    Serial.println(httpResponseCode);
 
   }

   Serial.print("Temperatura: ");
  
   // Convierte al temperatura a Centigrados.
   Serial.print(temp);
   Serial.println(" C");
   delay(2000);
 
   http.end();  //Free resources
 
  } else{
   
    Serial.println("Error in WiFi connection");   
   
  }
   
  delay(10000);  //Send a request every 10 seconds
 
}
